angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $q) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  $scope.firebaseAuthObj = new Firebase('https://brilliant-fire-2080.firebaseio.com/web/uauth');
  $scope.firebaseItemsObj = new Firebase('https://brilliant-fire-2080.firebaseio.com/items');

  // $scope.firebaseItemsObj.add({
    // name: 'bob'  
  // })


  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);
    $scope.firebase_auth($scope.loginData).then(function(user) {
        console.log(user);
    }, function(err) {
        console.log(err + " Login Unsuccessful");
    })
    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };

  $scope.firebase_auth = function(userObj) {
    // Handle Email/Password login
    // returns a promise
    var deferred = $q.defer();
    console.log(userObj);
    $scope.firebaseAuthObj.authWithPassword(userObj, function onAuth(err, user) {
        if (err) {
            deferred.reject(err);
        }

        if (user) {
            deferred.resolve(user);
        }
    });
    return deferred.promise;
  }

    // create a user but not login
    // returns a promsie
  /*
   function createUser(userObj) {
        var deferred = $.Deferred();
        rootRef.createUser(userObj, function (err) {

            if (!err) {
                deferred.resolve();
            } else {
                deferred.reject(err);
            }

        });

        return deferred.promise();
    }

    // Create a user and then login in
    // returns a promise
    function createUserAndLogin(userObj) {
        return createUser(userObj)
            .then(function () {
            return authWithPassword(userObj);
        });
    } 
*/
  var ref = new Firebase("https://brilliant-fire-2080.firebaseio.com");
  ref.createUser({
   email: "john@hi.com",
    password: "willy"
  }, function(error, userData) {
    if (error) {
      switch (error.code) {
        case "EMAIL_TAKEN":
          console.log("The new user account cannot be created because the email is already in use.");
          break;
        case "INVALID_EMAIL":
        console.log("The specified email is not a valid email.");
        break;
      default:
        console.log("Error creating user:", error);
    }
  } else {
    console.log("Successfully created user account with uid:", userData.uid);
  }
});
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});

//.controller('RegisterCtrl', function() {})
